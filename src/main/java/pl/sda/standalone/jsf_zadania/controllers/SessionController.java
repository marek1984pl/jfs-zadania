/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.jsf_zadania.controllers;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author Marek
 */
@Named(value = "sessionController")
@SessionScoped
public class SessionController implements Serializable {

    private int counter = 0;
    
    public SessionController() {
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
    
    public void increaseCounter() {
        counter++;
    }
}
