/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.jsf_zadania.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.*;
import pl.sda.standalone.jsf_zadania.model.Post;

/**
 *
 * @author Marek
 */
@ManagedBean(name = "applicationController")
@ApplicationScoped
public class ApplicationControllerBean {

    ArrayList<Post> postsList = new ArrayList();
    
    private final EntityManagerFactory emf;
    private final EntityManager em;
    
    public ApplicationControllerBean() {
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }
    
    public EntityManager getEntityManager() {
        return em;
    }
    
    private final String version = "0.1.4";

    public String getVersion() {
        return version;
    }

    public List<Post> getPostsList() {
        Query query = em.createQuery("from Post p");
        return query.getResultList();

        //return postsList;
    }
}
