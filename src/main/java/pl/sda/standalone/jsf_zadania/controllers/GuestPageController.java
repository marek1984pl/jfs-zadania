/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.jsf_zadania.controllers;

import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import pl.sda.standalone.jsf_zadania.model.Post;

/**
 *
 * @author Marek
 */
@ManagedBean(name = "guestPageController")
@RequestScoped
public class GuestPageController {
        
    @ManagedProperty(value = "#{applicationController}")
    private ApplicationControllerBean applicationController;
     
    private String controllerName = "Guest Controller";

    private String text;
    private String author;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    
    public ApplicationControllerBean getApplicationController() {
        return applicationController;
    }

    public void setApplicationController(ApplicationControllerBean applicationController) {
        this.applicationController = applicationController;
    }
    
    public GuestPageController() {
        System.out.println("***** Guest controller *****");
    }

    public String getControllerName() {
        return controllerName;
    }
    
    public void save() {

        EntityManager em = applicationController.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        Post post = new Post();
        post.setText(this.getText());
        post.setAuthor(this.getText());
        post.setCreateDate(new Date());
        em.persist(post);
        tx.commit();
        
        System.out.println("---- FORM SAVED! ----");
    }
}
